"""mind URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .auth import RegisterAPI_web,Login_web
from .user import Personal,Address,Plan
from .subscription import Subscription
from .payment import RazorpayEcomOrderWebsite,CallbackOrderRazorpay,RazorpayPaymentTransactionStatusApi
urlpatterns = [

    # Personal details
    path('personal/', Personal.as_view(),name="Personal_details"),

    # Address details
    path('address/', Address.as_view(),name="Address_details"),

    # Plan details
    path('plan/', Plan.as_view(),name="Plan_details"),

    # Subscription details
    path('subscription/', Subscription.as_view(),name="Subscription_details"),

    # Payment
    path('razorpay_order_website/',RazorpayEcomOrderWebsite.as_view(), name="Create_Payment_details"),
    path('callback_payment_razorpay/', CallbackOrderRazorpay.as_view(),name="CallBack_Payment_razorpay"),
    path('razorpay_transaction_status/', RazorpayPaymentTransactionStatusApi.as_view(),name="payment_transaction_status_check"),

    #auth
    path('register/', RegisterAPI_web.as_view(), name="Register"),
    path('login/', Login_web.as_view(), name="Login"),

]
