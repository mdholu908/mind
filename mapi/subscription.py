from rest_framework.authentication import *
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics, status


from .models import *
from . import user_serializers



class Subscription(generics.ListAPIView):
    """
    User Address Details API

    :parameter:
        Method: GET
        Response

        {
            'user':10
            "plan":2
            "start_date": "2019-09-13T11:48:12.077717",
            "expiry_date": "2019-10-13T11:48:12.077748",
            'payment_id':1
            'payment_status':"success"
           "created_date": "2019-09-13T11:48:12.077717",
            "updated_date": "2019-09-13T11:48:12.077748",
        }

        Method: POST
        :param
        {
            'user':10
            "plan":2
            'payment_status':"success"
        }

        :return
        {
            'user':10
            "plan":2
            "start_date": "2019-09-13T11:48:12.077717",
            "expiry_date": "2019-10-13T11:48:12.077748",
            'payment_id':1
            'payment_status':"success"
           "created_date": "2019-09-13T11:48:12.077717",
            "updated_date": "2019-09-13T11:48:12.077748",

        }

        :Note
        Payment status = ("initiated", "initiated"),
                         ("success", "success"),
                         ("failed", "failed"),
                         ("cancelled", "cancelled"),
                         ("cancelled_m", "cancelled manually"),
                         ("pending", "pending"),

        status 9 : No Details Found
        status 10 : Something went wrong

        # Status Code and Info
            HTTP_200_OK
            HTTP_201_CREATED
            HTTP_400_BAD_REQUEST
            HTTP_401_UNAUTHORIZED
            HTTP_403_FORBIDDEN

        CATEGORY	            DESCRIPTION
        1xx: Informational	Communicates transfer protocol-level information.
        2xx: Success	    Indicates that the client’s request was accepted successfully.
        3xx: Redirection	Indicates that the client must take some additional action in order to complete their request.
        4xx: Client Error	This category of error status codes points the finger at clients.
        5xx: Server Error	The server takes responsibility for these error status codes.



    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Subscription

    def post(self, request):

        request_data = request.data
        serializer_instance = self.serializer_class(data=request_data)

        if serializer_instance.is_valid():
            serializer_instance.save()
            return Response(serializer_instance.data, status=status.HTTP_201_CREATED)
        return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):

        try:

            user_profile_instance = Plan.objects.filter(status=True).order_by('-id')

            if user_profile_instance:
                return Response(
                    {
                        'user': user_profile_instance.user,
                        'plan': user_profile_instance.plan,
                        'payment_status': user_profile_instance.payment_status,
                        'payment_id': user_profile_instance.payment_id,
                        'start_date': user_profile_instance.start_date,
                        'expiry_date': user_profile_instance.expiry_date,

                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Address Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )





