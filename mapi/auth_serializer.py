
from django.core import validators
from rest_framework import serializers



class LoginRegisterSerializer(serializers.Serializer):
    """
    User login serializer
    """

    mobile_number = serializers.CharField(

        required=True,
        validators=[
            validators.RegexValidator('^[6-9]\d{9}$', message=''),
        ]
    )







