from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from rest_framework.authentication import *
from rest_framework.permissions import IsAuthenticated
from .auth_serializer import LoginRegisterSerializer

class RegisterAPI_web(APIView):
    """
        API For User Register
        :parameter:
            Medhod: POST
            Parameter
            {
                "mobile_number": "IntegerField",     # 9763630853
                "passwrod": "CharField",      # MU1122
            }
            Response
            {
                "status": "CharField",                  # 0
                "message": "ChatField",                 # OTP has been sent to 9763630853
            }
            Example:
            {
             "status": "1",
             "message": "OTP has been sent to 7875583679",
         }
            Note
            Guest token required
            guest-token : 'fae710e0-18bf-11e9-ab14-d663bd873d93'
            status 1 : User is  registered
            status 3 : User already registered and add invitation code
            status 10 : Entered data is in-valid (Validation Error)
            status 11 : Internal server error (Django Error)
    """
    serializer_class = LoginRegisterSerializer

    def post(self, request, *args, **kwargs):
        message = 'Something went wrong'
        user_login_serializer = self.serializer_class(data=request.data)
        if user_login_serializer.is_valid():
            # try:
            mobile_number = request.data.get('mobile_number')
            # invitation_code = request.data.get('invitation_code')
            password = request.data.get('password')
            user_instance = User.objects.filter(username=mobile_number)
            if not user_instance:
                User.objects.create(username=mobile_number,password=password)
                status = '1'


            else:
                status = '3'
                message = 'Already registered user cannot add invitation code'

        else:
            status = '10'
            message = 'Enter valid data'
        return Response(
            {
                'status': status,
                'message': message,
            }
        )






class Login_web(APIView):
    """
        API For User Login
        :parameter:
            Medhod: POST
            Parameter
            {
                "mobile_number": "IntegerField",     # 9763630853
                "passwrod": "CharField",      # MU1122
            }
            Response
            {
                "status": "CharField",                  # 0
                "message": "ChatField",                 # OTP has been sent to 9763630853
            }
            Example:
            {
             "status": "1",
             "message": "OTP has been sent to 7875583679",
         }
            Note
            Guest token required
            guest-token : 'fae710e0-18bf-11e9-ab14-d663bd873d93'
            status 1 : User is  registered
            status 3 : User already registered and add invitation code
            status 10 : Entered data is in-valid (Validation Error)
            status 11 : Internal server error (Django Error)
    """
    serializer_class = LoginRegisterSerializer

    def post(self, request, *args, **kwargs):
        message = 'Something went wrong'
        user_login_serializer = self.serializer_class(data=request.data)
        if user_login_serializer.is_valid():
            # try:
            mobile_number = request.data.get('mobile_number')
            # invitation_code = request.data.get('invitation_code')
            password = request.data.get('password')
            user_instance = User.objects.filter(username=mobile_number)
            if not user_instance:
                User.objects.filter(username=mobile_number,password=password)
                status = '1'


            else:
                status = '3'
                message = 'Already registered user cannot add invitation code'

        else:
            status = '10'
            message = 'Enter valid data'
        return Response(
            {
                'status': status,
                'message': message,
            }
        )

