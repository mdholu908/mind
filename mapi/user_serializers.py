from django.apps.registry import apps
from django.core.validators import RegexValidator
from rest_framework import serializers

from .models import *


class Personal(serializers.ModelSerializer):
    class Meta:
        model = Personal
        fields = ('__all__')



class Address(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('__all__')


class Plan(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = ('__all__')

