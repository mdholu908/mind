from rest_framework.views import APIView
from rest_framework.authentication import *
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics, status


from .models import *
from . import user_serializers
from django.http import HttpResponse


class Personal(generics.ListAPIView):
    """
    User Personal details API

    :parameter:
        Method: GET
        Response

        {
            'first_name':"CharField",
            'last_name':"CharField",
            'birthdate':"Datefield"
            'company ': "CharField",
        }



        Method: POST
        :param
            {
                 'first_name':"CharField",
            'last_name':"CharField",
            'birthdate':"Datefield"
            'company ': "CharField",
            }
        :return

            {
                "id": 12,
               'first_name':"Mohan",
                'last_name':"Dholu",
                'birthdate':"1994-0701"
                'company ': "Mudrakwik",
                "created_date": "2019-09-13T11:29:23.507956",
                "updated_date": "2019-09-13T11:29:23.507973",
                "user": 2
            }

        status 9 : No Details Found
        status 10 : Something went wrong

        # Status Code and Info
            HTTP_200_OK
            HTTP_201_CREATED
            HTTP_400_BAD_REQUEST
            HTTP_401_UNAUTHORIZED
            HTTP_403_FORBIDDEN

        CATEGORY	            DESCRIPTION
        1xx: Informational	Communicates transfer protocol-level information.
        2xx: Success	    Indicates that the client’s request was accepted successfully.
        3xx: Redirection	Indicates that the client must take some additional action in order to complete their request.
        4xx: Client Error	This category of error status codes points the finger at clients.
        5xx: Server Error	The server takes responsibility for these error status codes.

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Personal

    def get(self, request, *args, **kwargs):

        try:
            user_profile_instance = Personal.objects.filter(user=request.user.id).order_by('-id').first()

            if user_profile_instance:
                return Response(
                    {
                        'name': user_profile_instance.name,
                        'fathers_name': user_profile_instance.fathers_name,
                        'birthdate': user_profile_instance.birthdate,
                        'gender ': user_profile_instance.gender,
                        'marital_status': user_profile_instance.marital_status,
                        'occupation ': user_profile_instance.occupation,
                        'mobile_no ': user_profile_instance.mobile_no,
                        'alt_mobile_no': user_profile_instance.alt_mobile_no,
                        'email_id': user_profile_instance.email_id,
                        'monthly_income': str(user_profile_instance.monthly_income),
                        'pan_number': user_profile_instance.pan_number,
                        "itr": user_profile_instance.itr,
                        "mobile_linked_aadhaar": user_profile_instance.mobile_linked_aadhaar,
                        "residence_type": user_profile_instance.residence_type,
                        "industry_type": user_profile_instance.industry_type,
                        "educational": user_profile_instance.educational

                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Personal Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )

    def post(self, request):
        try:
            request_data = request.data
            request_data['user'] = request.user.id
            serializer_instance = user_serializers.Personal(data=request_data)
            if serializer_instance.is_valid():
                serializer_instance.save()
                return Response(serializer_instance.data, status=status.HTTP_201_CREATED)
            return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            print("Exception post Personal", e.args)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)










class Address(generics.ListAPIView):
    """
    User Address Details API

    :parameter:
        Method: GET
        Response

        {
            'address':
            'city':
            'state':
            'pin_code':
            'type':
            'user':

        }

        Method: POST
        :param
        {
            "address": "test",
            "city": "test",
            "state": "test",
            "pin_code": "test",
            "type": "Permanent"
        }

        :return
        {
            "id": 9,
            "address": "test",
            "city": "test",
            "state": "test",
            "pin_code": "test",
            "type": "Permanent",
            "created_date": "2019-09-13T11:48:12.077717",
            "updated_date": "2019-09-13T11:48:12.077748",
            "user": 2
        }

        :Note

        ADDRESS_TYPE = (
            ('Permanent', 'P'),
            ('Current', 'C')
        )

        status 9 : No Details Found
        status 10 : Something went wrong

        # Status Code and Info
            HTTP_200_OK
            HTTP_201_CREATED
            HTTP_400_BAD_REQUEST
            HTTP_401_UNAUTHORIZED
            HTTP_403_FORBIDDEN

        CATEGORY	            DESCRIPTION
        1xx: Informational	Communicates transfer protocol-level information.
        2xx: Success	    Indicates that the client’s request was accepted successfully.
        3xx: Redirection	Indicates that the client must take some additional action in order to complete their request.
        4xx: Client Error	This category of error status codes points the finger at clients.
        5xx: Server Error	The server takes responsibility for these error status codes.



    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Address

    def post(self, request):

        request_data = request.data
        request_data['user'] = request.user.id
        serializer_instance = self.serializer_class(data=request_data)

        if serializer_instance.is_valid():
            serializer_instance.save()
            return Response(serializer_instance.data, status=status.HTTP_201_CREATED)
        return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):

        try:

            user_profile_instance = Address.objects.filter(user=request.user.id).order_by('-id').first()

            if user_profile_instance:
                return Response(
                    {
                        'address': user_profile_instance.address,
                        'city': user_profile_instance.city,
                        'state': user_profile_instance.state,
                        'pin_code': user_profile_instance.pin_code,
                        'type': user_profile_instance.type,

                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Address Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )





class Plan(generics.ListAPIView):
    """
    User Address Details API

    :parameter:
        Method: GET
        Response

        {
            'amount':100
            'days':30
            'title':"plan 1 "
            'status':True
            'create_date':
            'update_date':

        }

        Method: POST
        :param
        {
            'amount':100
            'days':30
            'tital':"plan 1 "
            'status':True
        }

        :return
        {
            "id": 9,
            'amount':100
            'days':30
            'title':"plan 1 "
            'status':True
            "created_date": "2019-09-13T11:48:12.077717",
            "updated_date": "2019-09-13T11:48:12.077748",

        }

        :Note



        status 9 : No Details Found
        status 10 : Something went wrong

        # Status Code and Info
            HTTP_200_OK
            HTTP_201_CREATED
            HTTP_400_BAD_REQUEST
            HTTP_401_UNAUTHORIZED
            HTTP_403_FORBIDDEN

        CATEGORY	            DESCRIPTION
        1xx: Informational	Communicates transfer protocol-level information.
        2xx: Success	    Indicates that the client’s request was accepted successfully.
        3xx: Redirection	Indicates that the client must take some additional action in order to complete their request.
        4xx: Client Error	This category of error status codes points the finger at clients.
        5xx: Server Error	The server takes responsibility for these error status codes.



    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = user_serializers.Plan

    def post(self, request):

        request_data = request.data
        serializer_instance = self.serializer_class(data=request_data)

        if serializer_instance.is_valid():
            serializer_instance.save()
            return Response(serializer_instance.data, status=status.HTTP_201_CREATED)
        return Response(serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):

        try:

            user_profile_instance = Plan.objects.filter(status=True).order_by('-id')

            if user_profile_instance:
                return Response(
                    {
                        'amount': user_profile_instance.amount,
                        'days': user_profile_instance.days,
                        'status': user_profile_instance.status,
                        'title': user_profile_instance.title,

                    }
                )
            else:
                return Response(
                    {
                        'status': '9',
                        'message': 'No Details Found',

                    }
                )

        except Exception as e:
            print("-------User Address Details API-------", e.args)
            return Response(
                {
                    'status': '10',
                    'message': 'Something went wrong!'
                }
            )
















