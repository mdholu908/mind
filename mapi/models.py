from django.db import models
from mind import constants
# Create your models here.
from django.contrib.auth.models import User


class Personal(models.Model):
    """
    To store personal
    """
    first_name = models.CharField(db_index=True, max_length=256, blank=True, null=True)
    last_name = models.CharField(max_length=256, blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True)
    company = models.CharField(blank=True, null=True, max_length=250)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_personal")

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return '%s %s' % (self.name, self.user)




class Address(models.Model):
    """
    To store Address
    """
    address = models.CharField(max_length=256, null=True, blank=True)
    city = models.CharField(db_index=True, max_length=100, null=True, blank=True)
    state = models.CharField(db_index=True, max_length=100, null=True, blank=True)
    pin_code = models.CharField(db_index=True, max_length=10, null=True, blank=True)
    personal = models.ForeignKey(Personal, on_delete=models.CASCADE, null=True, blank=True,
                                 related_name="personal_address")
    type = models.CharField(db_index=True, blank=True, null=True, max_length=10, choices=constants.ADDRESS_TYPE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)







class Payment(models.Model):

    """
    To store payment
    """

    order_id = models.CharField(db_index=True,max_length=200, null=True, blank=True)
    transaction_id = models.CharField(db_index=True,max_length=200, null=True, blank=True)
    pg_transaction_id = models.CharField(max_length=50, null=True, blank=True)
    status = models.CharField(db_index=True,max_length=50, null=True, blank=True, choices=constants.STATUS_TYPE)
    category = models.CharField(db_index=True,max_length=50, null=True, blank=True, choices=constants.PAYMENT_CTG)
    mode = models.CharField(db_index=True,max_length=50, null=True, blank=True, choices=constants.PAYMENT_MODE)
    type = models.CharField(db_index=True,max_length=50, null=True, blank=True, choices=constants.PAYMENT_Type)
    response = models.TextField(blank=True, null=True)
    amount = models.FloatField(db_index=True,max_length=50, null=True, blank=True)
    description = models.CharField(max_length=256, blank=True, null=True)
    date = models.DateTimeField(db_index=True,null=True, blank=True)
    pay_source = models.CharField(db_index=True, null=True, blank=True, max_length=50, choices=constants.PaymentGateway)
    return_status = models.CharField(db_index=True, null=True, blank=True, max_length=50,
                                     choices=constants.ReturnStatus)
    response_code = models.CharField(db_index=True, null=True, blank=True, max_length=50)
    product_type = models.CharField(default="Mind", max_length=256, blank=True, null=True, choices=constants.PRODUCT_TYPE)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null=True, blank=True)





class Plan(models.Model):
    """
    To store plan
    """
    amount = models.FloatField(null=True,blank=True)
    days = models.IntegerField(null=True,blank=True)
    title = models.CharField(null=True,blank=True,max_length=250)
    status = models.BooleanField(default=True,null=True,blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True, null=True, blank=True)


class Subscription(models.Model):
    """
        To store Plan subscription
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_subscription", blank=True, null=False)
    start_date = models.DateTimeField(db_index=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    expiry_date = models.DateTimeField(null=True)
    payment_status = models.CharField(db_index=True, max_length=50, null=True,choices=constants.STATUS_TYPE)
    payment_id = models.ForeignKey(Payment, on_delete=models.CASCADE, related_name="subscription_payment", null=True)
    plan =  models.ForeignKey(Plan, on_delete=models.CASCADE, related_name="subscription_plan", null=True)

    class Meta:
        ordering = ['-id']









