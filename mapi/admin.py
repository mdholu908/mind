from django.contrib import admin

# Register your models here.


from .models import *




class PersonalDetailsAdmin(admin.ModelAdmin):
    list_display = (
        'first_name','last_name', 'birthdate', 'company','user')
    exclude = ('updated_date',)
    search_fields = ('user__username',)
    list_filter = ('first_name', 'last_name',)



class AddressAdmin(admin.ModelAdmin):
    list_display = ('personal', 'address', 'city', 'state', 'pin_code', 'type',)
    exclude = ('created_date',)
    search_fields = ('personal__user__username', 'city', 'state', 'pin_code', 'type',)
    list_filter = ('personal', 'city', 'state', 'pin_code', 'type',)



class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('user', 'start_date', 'expiry_date', 'payment_status', 'payment_id','plan')
    exclude = ('created_date',)
    search_fields = ('user__username', 'start_date', 'expiry_date',)
    list_filter = ('user', 'payment_status',)




class PlanAdmin(admin.ModelAdmin):
    list_display = ('amount', 'days', 'status', 'title',)
    exclude = ('created_date',)
    search_fields = ('amount', 'days', 'status',)
    list_filter = ('amount', 'status','days',)




class PaymentAdmin(admin.ModelAdmin):

    list_display = (
        'order_id','transaction_id', 'pg_transaction_id', 'status',
        'category', 'mode', 'type','date','create_date',
        'amount',
    )

    list_filter = (
        'status',
        'category', 'mode', 'type', 'date',

    )

    search_fields = ('order_id','transaction_id', 'status',
        'category', 'mode', 'type','date',
        'amount',)



admin.site.register(Personal,PersonalDetailsAdmin)
admin.site.register(Address,AddressAdmin)
admin.site.register(Subscription,SubscriptionAdmin)
admin.site.register(Plan,PlanAdmin)
admin.site.register(Payment,PaymentAdmin)



