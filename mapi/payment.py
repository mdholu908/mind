from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime,timedelta
import razorpay
import hmac
import hashlib
import random
from .models import *
from django.utils.decorators import method_decorator
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import *
from rest_framework.permissions import IsAuthenticated



# Test cred
Key_id = 'rzp_test_p5riAqcssED1JK'
Key_secret = 'PQ26fiDi5sk9E1wwwRCI0wF5'



website_base_url =  "https://mind.com/"



@method_decorator(csrf_exempt, name='dispatch')
class CallbackOrderRazorpay(APIView):
    """
    this api call in payment  response in server side
     :param data:
     {
     "razorpay_payment_id":"fddfdfdfdfd",
     "razorpay_order_id":"order_eredsddcvc",
     "razorpay_signature":"dfdvlmmclvmdvfdmfldmfldfmldfdlfd",
     }
    :return:
    {
    redirect websiter url return
    }
    """
    def post(self, request, *args, **kwargs):
        #def callback_order_razorpay(request):
        # redirect_url = 'http://15.207.21.5:3000/account/process_payment?order_id='
        redirect_url = website_base_url + "account/process_payment?order_id="
        print("test data ---- ",request.data)
        data = request.data
        razorpay_payment_id = data["razorpay_payment_id"]
        razorpay_order_id = data["razorpay_order_id"]
        razorpay_signature = data["razorpay_signature"]
        payment = Payment.objects.filter(pg_transaction_id=razorpay_order_id)
        if payment:
            order_id_s=str(payment[0].order_id).split("e")
            order_id = order_id_s[0]
            data ={
                "order_id":order_id,
                "razorpay_payment_id":razorpay_payment_id,
                "razorpay_order_id":razorpay_order_id,
                "razorpay_signature":razorpay_signature,
                "pay_order_id":payment[0].order_id
            }
            if order_id is not None:
                Order_transaction_Status_Check_auto(data)
                redirect_with_oid = redirect_url + str(payment[0].order_id)
                return redirect(redirect_with_oid)
        return redirect(website_base_url + "account/payment")










def order_status(razorpay_order_id):
    """
    this function check order status in razorpay
    :param razorpay_order_id:
    :return:
    """
    resp_data = {}
    if razorpay_order_id and razorpay_order_id is not None:
        chk_order_id = razorpay_order_id
        client = razorpay.Client(auth=(Key_id, Key_secret))
        order = client.order.fetch(chk_order_id)
        if order:
            resp_data = {
                'order_id': order['id'],
                'amount': str(order['amount']),
                'amount_paid': order['amount_paid'],
                'status': order['status'],
                'receipt': order['receipt'],
                'created_at': order['created_at'],
                'currency': order['currency'],
                'image_url': '',
                'key_id': Key_id
            }

            return resp_data

    else:
        return resp_data


def payment_status(razorpay_payment_id):
    """
    this function check payment status in razorpay
    :param razorpay_payment_id:
    :return:
    """
    resp_data = {}
    if razorpay_payment_id and razorpay_payment_id is not None:
        chk_payment_id = razorpay_payment_id
        client = razorpay.Client(auth=(Key_id, Key_secret))
        payment = client.payment.fetch(chk_payment_id)
        if payment:
            resp_data = {
                'payment_id': payment['id'],
                'entity': payment['entity'],
                'amount': str(payment['amount']),
                'currency': str(payment['currency']),
                'status': str(payment['status']),
                'order_id': str(payment['order_id']),
                'invoice_id': payment['invoice_id'],
                'international': payment['international'],
                'method': payment['method'],
                'amount_refunded': payment['amount_refunded'],
                'refund_status': payment['refund_status'],
                'captured': payment['captured'],
                'description': payment['description'],
                'card_id': payment['card_id'],
                'bank': payment['bank'],
                'wallet': payment['wallet'],
                'vpa': payment['vpa'],
                'email': payment['email'],
                'contact': payment['contact'],
                'notes': payment['notes'],
                'fee': payment['fee'],
                'tax': payment['tax'],
                'error_code': payment['error_code'],
                'error_description': payment['error_description'],
                'created_at': payment['created_at'],
                'key_id': Key_id
            }
            return resp_data

    else:
        return resp_data






def Order_transaction_Status_Check_auto(data):
    """
    razorpay payment status  check function
    :param data:
    :return:
    """
    message = ''
    mode = ''
    status = 0
    pay_status = None
    razorpay_payment_id = data['razorpay_payment_id']

    razorpay_order_id = data['razorpay_order_id']
    razorpay_signature = data['razorpay_signature']
    # generated_signature = hmac_sha256(razorpay_order_id + "|" + razorpay_payment_id, secret);
    client = razorpay.Client(auth=(Key_id, Key_secret))
    msg = "{}|{}".format(str(data['razorpay_order_id']), str(data['razorpay_payment_id']))

    secret = str(Key_secret)

    key = bytes(secret, 'utf-8')
    body = bytes(msg, 'utf-8')

    dig = hmac.new(key=key,
                   msg=body,
                   digestmod=hashlib.sha256)

    generated_signature = dig.hexdigest()
    result = hmac.compare_digest(generated_signature, razorpay_signature)
    order_text = data["order_id"]
    if result == True:
        pass


    order_resp_data = order_status(razorpay_order_id)
    if order_resp_data and order_resp_data is not None:
        status = 2
        message = " Sign Verification Failed"

    payment_data = payment_status(razorpay_payment_id)
    if payment_data and payment_data is not None:
        if payment_data['method'] == 'card':
            mode = 'DC'
        elif payment_data['method'] == 'upi':
            mode = 'UPI'
        elif payment_data['method'] == 'netbanking':
            mode = 'NB'
        else:
            mode = 'RWT'
        timestamp = payment_data['created_at']
        try:
            timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

        except Exception as e:
            print(e)
            pass

        if payment_data['status'] == 'captured':
            pay_status = 'success'
            status = '1'
            message = 'Transaction successful.'

        elif payment_data['status'] == 'authorized':
            pay_status = 'pending'

            status = '3'
            message = 'Transaction pending'



        else:
            pay_status = 'failed'
            status = '2'
            message = 'Transaction failed'

        Payment.objects.filter(order_id=data["pay_order_id"],
                               category='Subscription').update(status=pay_status,
                                                        transaction_id=data["razorpay_payment_id"],mode=mode,
                                                        update_date=datetime.now())
        payment_details_obj = Payment.objects.filter(order_id=data["pay_order_id"],
                               category='Subscription')
        if payment_details_obj:
                Subscription.objects.filter(payment_id=payment_details_obj[0].id).update(payment_status=pay_status)

    return  status







class RazorpayEcomOrderWebsite(APIView):
    """
        Paytm Order Checksum API
        documentation Url: "https://developer.paytm.com/docs/v1/payment-gateway/#demo"
    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status = None
        message = None
        salt = None
        ctx = {}
        try:

            plan_amount = request.data.get('amount')
            date = datetime.datetime.now()
            random_number = random.sample(range(0, 9999), 1)
            ORDER_ID = str(date.year)[2:] + "" + str(date.month) + "" + str(date.day) + "" + str(
                date.hour) + "" + str(date.minute) + "" + str(date.second) + "" + str(random_number[0])


            url = "https://mind.com/mapi/callback_order_razorpay/"
            TXN_AMOUNT = str(request.data.get('TXN_AMOUNT'))
            amount = float(TXN_AMOUNT) * 100
            data = {'amount': amount, 'currency': 'INR', 'receipt': ORDER_ID, 'payment_capture': '1'}
            client = razorpay.Client(auth=(Key_id, Key_secret))
            order = client.order.create(data)
            personal = Personal.objects.filter(user_id=request.user.id).last()
            name=None
            dec=None
            email_id=None
            mobile_no=None
            real_amount = order['amount'] / 100
            if personal:
                name=personal.name
                dec=personal.name
                email_id=personal.email_id
                mobile_no=personal.user.username
            if order:
                ctx['order'] = order
                resp_data = {
                    'order_id': order['id'],
                    'amount': str(real_amount),
                    'status': order['status'],
                    'created_at': order['created_at'],
                    'currency': order['currency'],
                    'image_url': '',
                    'key_id': Key_id,
                    'name':name,
                    'dec':dec,
                    'callbackurl':url,
                    'order_id_ck':ORDER_ID,
                    'email_id':email_id,
                    'mobile_no':mobile_no,

                }

                timestamp = order['created_at']
                try:
                    timestamp = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

                except Exception as e:
                    print(e)
                    pass



                paym=Payment.objects.create(order_id=ORDER_ID, pg_transaction_id=resp_data['order_id'], \
                                        status="initiated", amount=real_amount, \
                                        category="Subscription", date=timestamp, return_status="rstatus",
                                        pay_source="Rpay")
                if paym and paym is not None:
                    payment_details_obj = Payment.objects.filter(order_id=ORDER_ID,
                                                                 category='Subscription')
                    if payment_details_obj:
                        plan = Plan.objects.filter(amount=plan_amount)
                        if plan:
                            start_date = datetime.now()
                            date_time_str = str(start_date)
                            date_time_obj = datetime.datetime.strptime(date_time_str[:19], '%Y-%m-%d %H:%M:%S')
                            expiry_date = date_time_obj + timedelta(89)
                            Subscription.objects.create(payment_id=payment_details_obj[0].id,plan=plan,start_date=start_date,expiry_date=expiry_date, payment_status="initiated")

                status = '1'
                message = 'Success'

                return Response(
                    {
                        'status': status,
                        'message': message,
                        'data': resp_data
                    }
                )


        except Exception as e:
            print("-----Exception----in checksum web", e)
            import sys
            import os
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return Response(
                {
                    'status': status,
                    'message': message
                }
            )








class RazorpayPaymentTransactionStatusApi(APIView):
    """
       This API shows the transaction status of the payment..
    """

    def post(self, request, *args, **kwargs):
        import requests
        status = None
        message = None
        amount = 0.0
        date_strip = None
        try:
            print('inside try===========')
            order_id = request.data.get('order_id')
            print('order_id============', order_id)
            if order_id:
                pay=Payment.objects.filter(order_id=order_id).last()
                if pay:
                        return Response(
                            {'status_code': 1,
                             'message': "Order fetch Successfully",
                             'ecom_order_id': order_id,
                             'paytm_order_id': order_id,
                             'transaction_id':pay.transaction_id,
                             'status': pay.status,
                             'date': pay.date,
                             'amount': pay.amount,
                             }
                        )
                else:
                    return Response(
                        {'status_code': 2,
                         'message': "Something went wrong",
                         'ecom_order_id': '',
                         'paytm_order_id': order_id,
                         'transaction_id': None,
                         'status': '',
                         'date': date_strip,
                         'amount': amount,
                         }
                    )




        except Exception as e:
            import sys
            import os
            print('-----------in exception----------')
            print(e.args)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)




