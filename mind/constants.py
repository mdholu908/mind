

ADDRESS_TYPE = (
    ('Permanent', 'P'),
    ('Current', 'C'),
)

# For Payment

STATUS_TYPE = (
    ("initiated", "initiated"),
    ("success", "success"),
    ("failed", "failed"),
    ("cancelled", "cancelled"),
    ("cancelled_m", "cancelled manually"),
    ("pending", "pending"),
)

PAYMENT_MODE = (
    ('NB', 'Net Banking'),
    ('DC', 'Debit Card'),
    ('CC', 'Credit Card'),
    ('UPI', 'UPI'),
    ('PPI', 'Wallet'),
    ('RWT', 'Rzpay_Wallet'),
    ('C', 'Cash'),
    ('NA', 'Cancel'),
)

PAYMENT_SUB_MODE = 'IMPS'

PAYMENT_CTG = (
    # Payment Category
    ('Subscription', 'Subscription'),

)

PRODUCT_TYPE = (
    ('Mind', 'Mind'),

)

PaymentGateway = (
    # From which payment gateway this transaction is made
    ('Ptm', 'Ptm'),
    ('Rpay', 'Rpay'),
)
ReturnStatus = (
    # From which payment gateway this transaction is made
    ('ps2s', 'ps2s'),
    ('pstatus', 'pstatus'),
    ('rstatus', 'rstatus'),
    ('rs2s', 'rs2s'),
)

PAYMENT_Type = (
    # Payment Type
    ('Cash', 'Cash'),
    ('Account', 'Account'),
    ('App', 'App'),
    ('Web', 'Web'),
)


COM = (
    ('Yes', 'Yes'),
    ('No', 'No'),
)
